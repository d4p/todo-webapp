<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
		<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">
		<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
        <title>Strona logowania</title>
    </head>
    <body>
    <div class="container" style="width: 300px">
    <form action="login" method="POST">
    <h2 class="form-signin-heading">Logowanie:</h2>
    <input type="text" class="form-control" placeholder="Nazwa użytkownika" required autofocus name="userLogin" /><br />
    <input type="password" class="form-control" placeholder="Hasło" required name="userPassword" /><br />
     <button class="btn btn-lg btn-primary btn-block" type="submit">Zaloguj się</button><br />
    </form>
    <a href="register.jsp">Zarejestruj się</a>
    </div>
    </body>
</html>
