<%@page import="pl.edu.pjwstk.gdansk.s10908.jaz.todo.service.AuthenticationService"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
<title>Lista zadań</title>
</head>
<body>
<a href="logout" class="pull-right btn btn-default" style="margin: 10px;">Wyloguj</a>
<div class="container page-header">
  <h1><span class="glyphicon glyphicon-user"></span> Witaj ${login.currentUser.login}!<br /><br /><small>Oto Twoja lista zadań: </small></h1>
</div>
<div class="container">
${todo.userTaskTable}
</div>
<br />
<div class="container" style="width: 500px">
<form action="todo-add" method="POST">
<div class="form-group">
Priorytet
<select class="form-control"  name="todoPriority">
  <option value=10>Ważne</option>
  <option value=5>Zwykłe</option>
  <option value=0>Mało ważne</option>
</select></div> 
<div class="form-group"><input type="text" class="form-control" placeholder="Tytuł" required autofocus name="todoTitle" /></div>
<div class="form-group"><input class="form-control" placeholder="Opis" required name="todoDescription" /></div>
<button class="btn btn-lg btn-primary btn-block" type="submit">Dodaj zadanie</button><br />
</form>
</div>
</body>
</html>	