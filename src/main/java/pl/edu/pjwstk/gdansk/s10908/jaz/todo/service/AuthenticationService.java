package pl.edu.pjwstk.gdansk.s10908.jaz.todo.service;

import javax.ejb.Local;

import pl.edu.pjwstk.gdansk.s10908.jaz.todo.domain.User;

@Local
public interface AuthenticationService {

    public User getCurrentUser();

    public void setCurrentUser(User currentUser);

    public User login(String login, String password);

    public String register(String login, String password1, String password2);

    public boolean isUserLogged();
}
