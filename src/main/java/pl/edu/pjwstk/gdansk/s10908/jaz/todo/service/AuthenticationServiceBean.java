package pl.edu.pjwstk.gdansk.s10908.jaz.todo.service;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;

import pl.edu.pjwstk.gdansk.s10908.jaz.todo.domain.Priority;
import pl.edu.pjwstk.gdansk.s10908.jaz.todo.domain.Task;
import pl.edu.pjwstk.gdansk.s10908.jaz.todo.domain.User;

@Stateless
public class AuthenticationServiceBean implements AuthenticationService {

    private Collection<User> userDatabase;
    private MessageDigest sha256;

    private User currentUser;

    @PostConstruct
    private void init() {
	try {
	    sha256 = MessageDigest.getInstance("SHA-256");
	} catch (NoSuchAlgorithmException e) {
	    e.printStackTrace();
	}

	userDatabase = new ArrayList<User>();
	User user0 = new User();
	user0.setLogin("user0");
	user0.setPassword(Arrays.toString(sha256.digest("qwerty".getBytes())));
	Task[] tasks0 = {
		new Task(new Priority("Ważne", 10), "Jaz 1st Java webapp project",
			"Project using jsp, servlets, session..."),
		new Task(new Priority("Zwykłe", 5), "Ex. title", "desc..."),
		new Task(new Priority("Mało ważne", 5), "Another title", "desc2..."),

	};
	user0.setTodo(new ArrayList<Task>());
	user0.getTodo().addAll(Arrays.asList(tasks0));
	userDatabase.add(user0);
    }

    @Override
    public User getCurrentUser() {
	return currentUser;
    }

    @Override
    public void setCurrentUser(User currentUser) {
	this.currentUser = currentUser;
    }

    @Override
    public User login(String login, String password) {
	String pass = Arrays.toString(sha256.digest(password.getBytes()));
	for (User user : userDatabase) {
	    if (user.getLogin().equals(login) && user.getPassword().equals(pass)) {
		currentUser = user;
		return user;
	    }
	}
	return null;
    }

    @Override
    public boolean isUserLogged() {
	return (currentUser != null);
    }

    @Override
    public String register(String login, String password1, String password2) {
	String returnLink = " <a href=\"register.jsp\">Powrót</a>";
	if (password1 == null) {
	    return "ERROR: Hasło nie jest wpisane" + returnLink;
	} else if (!password1.equals(password2)) {
	    return "ERROR: Wpisane hasła nie są ze sobą zgodne" + returnLink;
	} else if (password1.length() < 5) {
	    return "ERROR: Długość hasła musi być dłuższa niż 5 znaków" + returnLink;
	} else {
	    User userDTO = new User(login, password1, new ArrayList<Task>());
	    for (User user : userDatabase) {
		if (user.getLogin().equals(userDTO)) {
		    return "ERROR: Użytkownik o nazwie " + userDTO.getLogin() + " już istnieje. "
			    + returnLink;
		}
	    }
	    userDatabase.add(userDTO);
	    currentUser = userDTO;
	    return null;
	}
    }
}
