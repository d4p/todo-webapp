package pl.edu.pjwstk.gdansk.s10908.jaz.todo.web;

import java.io.IOException;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import pl.edu.pjwstk.gdansk.s10908.jaz.todo.service.TodoService;

@WebServlet(urlPatterns = "/todo-add")
public class TodoServlet extends HttpServlet {

    private static final long serialVersionUID = 8797038918088924054L;

    @EJB
    private TodoService todoService;

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, IOException {
	int todoPriority = Integer.parseInt(request.getParameter("todoPriority"));
	String priorityName = "UNDEFINED";
	switch (todoPriority) {
	case 10:
	    priorityName = "WAŻNE";
	    break;
	case 5:
	    priorityName = "ZWYKŁE";
	    break;
	case 0:
	    priorityName = "MAŁO WAŻNE";
	    break;
	}

	if (todoService.addTask(priorityName, todoPriority, request.getParameter("todoTitle"),
		request.getParameter("todoDescription"))) {
	    response.sendRedirect(request.getContextPath() + "/todo.jsp");
	} else {
	    response.sendRedirect(request.getContextPath() + "/todo-error.jsp");
	}
    }

}
