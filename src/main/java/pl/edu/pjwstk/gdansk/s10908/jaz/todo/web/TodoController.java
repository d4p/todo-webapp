package pl.edu.pjwstk.gdansk.s10908.jaz.todo.web;

import java.util.ArrayList;
import java.util.Collection;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

import pl.edu.pjwstk.gdansk.s10908.jaz.todo.domain.Task;
import pl.edu.pjwstk.gdansk.s10908.jaz.todo.service.TodoService;

@RequestScoped
@Named("todo")
public class TodoController {

    @EJB
    private TodoService todoService;

    private Collection<Task> userTasks;

    private String userTaskTable;

    @PostConstruct
    private void init() {
	userTasks = todoService.getUserTaskCollection();

	if (userTasks == null) {
	    userTasks = new ArrayList<Task>();
	}
	userTaskTable = generateTaskTable();
    }

    private String generateTaskTable() {
	String table = "";

	if (userTasks.size() != 0) {
	    String trClass = "";
	    table += "\n<table class=\"table table-responsive\">\n"
		    + "<tr><th>Priorytet</th><th>Tytuł</th><th>Opis</th></tr>";
	    for (Task task : userTasks) {
		switch (task.getPriority().getValue()) {
		case 10:
		    trClass = "danger";
		    break;
		case 5:
		    trClass = "warning";
		    break;
		case 0:
		    trClass = "info";
		    break;
		}
		table += "\n<tr class=\"" + trClass + "\"><td>" + task.getPriority().getName()
			+ "</td><td>" + task.getTitle() + "</td><td>" + task.getDescription()
			+ "</td></tr>";
	    }
	    table += "</table>";
	}
	return table;
    }

    public Collection<Task> getUserTasks() {
	return userTasks;
    }

    public void setUserTasks(Collection<Task> userTasks) {
	this.userTasks = userTasks;
    }

    public String getUserTaskTable() {
	return userTaskTable;
    }

    public void setUserTaskTable(String userTaskTable) {
	this.userTaskTable = userTaskTable;
    }

}
